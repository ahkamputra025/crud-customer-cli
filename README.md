# CRUD Customer CLI
Simple CRUD CLI with nodeJS and Mongodb

# Getting started
-  Add your connection string in `main.js` line 7

### Installation
Install the dependencies

```sh
$ npm install
```

### Create Symlink

```sh
$ npm link
```

### Commands

List Customers (list or l)
```sh
$ client-cli list
```

Find Customers (find or f)
```sh
$ client-cli find [NAME]
```

Add Customer (add or a)
```sh
$ client-cli add
```

Update Customer (update or u)
```sh
$ client-cli update [_ID]
```

Remove Customer (remove or r)
```sh
$ client-cli remove [_ID]
```
