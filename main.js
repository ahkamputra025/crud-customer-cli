const mongoose = require('mongoose');

//Map global promise - get rid of warning
// mongoose.Promise = global.Promise;

//connect to db
const db = mongoose.connect('<fill it with your MongoDB connection link>', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

//import model
const Customer = require('./models/customer');

//add customer
const addCustomer = (customer) => {
    Customer.create(customer).then(customer => {
        console.info('New Customer Added');
        mongoose.connection.close();
    });
}

//find customer
const findCustomer = (name) => {
    //make case insensitive
    const search = new RegExp(name, 'i');
    Customer.find({$or: [{firstname: search}, {lastname: search}]}).then(customer => {
        console.info(customer);
        console.info(`${customer.length} matches`);
        mongoose.connection.close();
    });
}

//update customer
const updateCustomer = (_id, customer) => {
    Customer.updateOne({_id}, customer)
        .then(customer => {
            console.info('Customer Updated');
            mongoose.connection.close();
        });
}

//remove customer
const removeCustomer = (_id) => {
    Customer.deleteOne({_id})
        .then(customer => {
            console.info('Customer Removed');
            mongoose.connection.close();
        });
}

//list customer all
const listCustomers = () => {
    Customer.find()
        .then(customers => {
            console.info(customers);
            console.info(`${customers.length} customers`);
            mongoose.connection.close();
        });
}

// Export All methods
module.exports = {
    addCustomer,
    findCustomer,
    updateCustomer,
    removeCustomer,
    listCustomers
};